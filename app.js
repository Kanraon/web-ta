const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const dbConfig = require(__dirname+'/config/db.config.js');

// Getting data from Mysql database
var mysql = require('mysql');
var con = mysql.createPool({
    host: dbConfig.remote.HOST, // servername
    user: dbConfig.remote.USER, // mysql username
    password: dbConfig.remote.PASSWORD, // mysql password
    database: dbConfig.remote.DB
});

var raw_material = "";
var pallet_material = "";
function queryServer() {
 
    con.getConnection((err, connection) => {
        if(err) throw err;
        let query = "SELECT jenis_material,stok FROM `stokmaterial` ORDER BY reading_time DESC LIMIT 1";
        connection.query(query, (err, result) => {
         // return the connection to pool
            if(err) throw err;
            raw_material = result[0];
            let query = "SELECT uid,material_type,material_amount stok FROM `paletdata` ORDER BY reading_time DESC LIMIT 1";
            connection.query(query, (err, result) => {
                connection.release(); // return the connection to pool
                if(err) throw err;
                pallet_material = result[0];
            });
        });
    });

  
};

function getData(queryServer,interval){
    queryServer();
    return setInterval(queryServer, interval);
};
//getData(queryServer,5000);

// Using ejs
app.set("view engine", "ejs");

// Display HTML
app.use(bodyParser.urlencoded({
    extended: true
}));
queryServer();
app.use(express.static("public"));
app.get("/", function (req, res) {
    //res.sendFile(__dirname + '/index.html');
    con.getConnection((err, connection) => {
        if(err) throw err;
        let query = "SELECT jenis_material,stok,reading_time FROM `stokmaterial` ORDER BY reading_time DESC";
        connection.query(query, (err, result) => {
         // return the connection to pool
            if(err) throw err;
            raw_material = result;
            let query = "SELECT uid,material_type,material_amount,reading_time FROM `paletdata` ORDER BY reading_time DESC ";
            connection.query(query, (err, result) => {
                connection.release(); // return the connection to pool
                if(err) throw err;
                pallet_material = result;
                res.render("index", {
                    rawData : JSON.stringify(raw_material),
                    palletData : JSON.stringify(pallet_material)
                });
                let formatTime = new Date(pallet_material[0].reading_time);
                //console.log(dbConfig.HOST);
            });
        });
    });

});
// app.get("/test",function(req,res){
//     con.getConnection((err, connection) => {
//         if(err) throw err;
//         let query = "SELECT jenis_material,stok FROM `stokmaterial` ORDER BY reading_time DESC";
//         connection.query(query, (err, result) => {
//             connection.release(); // return the connection to pool
//             if(err) throw err;
//             pallet_material = JSON.stringify(result);
//             console.log(result);
//             res.render("test",{
//                 pallet_material : pallet_material
//             });
//         });
//     });
    
// })
app.listen(process.env.PORT||3000, function (req, res) {
    console.log("Server started on port 3000");
})

  