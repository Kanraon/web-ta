$(document).ready(function () {
  setInterval(function () {
    cache_clear()
  }, 30000);

});

function cache_clear() {
  window.location.reload(true);
  // window.location.reload(); use this if you do not remove cache
}
// Parse Data from Server
Date.prototype.yyyymmdd = function () {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [(dd > 9 ? '' : '0') + dd, '-',
    (mm > 9 ? '' : '0') + mm, '-',
    this.getFullYear()
  ].join('');
};
Date.prototype.secminhour = function () {
  var sec = this.getSeconds();
  var min = this.getMinutes();
  var hours = this.getHours();

  return [(hours > 9 ? '' : '0') + hours, ':',
    (min > 9 ? '' : '0') + min, ':', (sec > 9 ? '' : '0') + sec
  ].join('');
};